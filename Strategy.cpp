// ���������.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
//����������� ����� Greeter
class Greeter {
public:
	Greeter(){}
	~Greeter(){}

	virtual void use() = 0;
};
//������ ���������� ���������� - ���������
class Greeter_1: public Greeter {
public:
	Greeter_1(){}
	~Greeter_1(){}

	void use(){ cout << "Hello" << endl; };
};

class Greeter_2: public Greeter {
public:
	Greeter_2(){}
	~Greeter_2(){}

	void use(){ cout << "Hi" << endl; };
};

class Greeter_3: public Greeter {
public:
	Greeter_3(){}
	~Greeter_3(){}

	void use(){ cout << "Hey" << endl; };
};

class Context {
private:
	Greeter* _operation;

public:
	Context(){}
	~Context(){}

	void UseGreeter(){
		_operation->use(); // ���������� ������ ���������
	};
	void SetGreeter(Greeter* privet) {
		_operation = privet; //�������� ������ ���������
	};
};



int _tmain(int argc, _TCHAR* argv[])
{
	Context customClient;
	Greeter_1 *str1 = new Greeter_1();
	Greeter_2 *str2 = new Greeter_2();
	Greeter_3 *str3 = new Greeter_3();
	//������������� ������ ��������� � ����������
	customClient.SetGreeter(str1);
	customClient.UseGreeter();	
	customClient.SetGreeter(str2);
	customClient.UseGreeter();
	customClient.SetGreeter(str3);
	customClient.UseGreeter();

	return 0;
}